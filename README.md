> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Docker image: Apache avec le support FPM

[![pipeline status](https://gitlab.com/jordanprotin/docker/apache/badges/master/pipeline.svg)](https://gitlab.com/jordanprotin/docker/apache/commits/master)

# Sommaire

- [Introduction](#introduction)
- [Tags actuellement disponibles](#tags-actuellement-disponibles)
- [Configuration](#configuration)

# Introduction

Il s'agit d'une image docker construite à partir de l'image officielle [httpd](https://hub.docker.com/_/httpd/).
Cela permet d'ajouter le support pour **php-fpm**, qui permet de protéger le contenu avec une authentification HTTP basique et peut également bloquer les robots pour l'indexation de contenu.

> :+1: Toute la [configuration](#configuration) peut être paramétrée en utilisant les variables d'environnements.

Les changements sont appliquées sans devoir reconstruire l'image.

# Tags actuellement disponibles

> **Pull command** : 
> docker pull [registry.gitlab.com/jordanprotin/docker/apache:2.4](https://gitlab.com/jordanprotin/docker/apache/container_registry)

* 2.4 (latest)

# Configuration

La configuration peut-être paramétrée en passant les variables d'environnements (cf. tableau ci-dessous) en paramètres de la commande suivante  :

```bash
$ docker run -e DISALLOW_ROBOTS=false -d registry.gitlab.com/jordanprotin/docker/apache
```

| Nom de la variable        | Description                                                        | Valeur par défaut |
|-----------------|--------------------------------------------------------------------|---------------|
| PHP_HOST        | IP ou hostname où PHP fpm s'éxecute.                           | php-fpm       |
| PHP_PORT        | Le port PHP fpm.                                                      | 9000          |
| PHP_PATH        | Racine du document root à la fois pour apache et php-fpm (sans les /)| /var/www/html |
| DISALLOW_ROBOTS | Si true, un robots.txt est utilisé pour interdire l'accès aux moteurs de recherche.  | true          |
| HT_PASSWORD     | Si définie, une autorisation est requise pour accéder au serveur.         | (empty)       |
| HT_LOGIN        | Login par défaut utilisé seulement si le password est défini.                  | anonymous     |
| PROXY_TIMEOUT   | Délai timeout pour les requêtes proxy PHP.                              | 60            |
